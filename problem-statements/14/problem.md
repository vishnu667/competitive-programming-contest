### Problem Statement 14

You are given a sequence S of  n, not necessarily distinct numbers x<sub>1</sub>, x<sub>2</sub>, …, x<sub>n</sub>. You are also given an integer k where 1 <= k < n. An inversion of the sequence x<sub>1</sub>, x<sub>2</sub>, …, x<sub>n</sub> occurs when i < j, and x<sub>i</sub> > x<sub>j</sub>.


Consider all subsequences of length k of the sequence S.  Clearly the number of such subsequences is n - k + 1. 


Denote each subsequence by A(m) where m is an index enumerating the subsequences. So m ranges from 1 to n.


For each subsequence A(m), let M<sub>m</sub> be the number of inversions in the subsequence A(m).


Find the maximum value of M<sub>m</sub>


**Example:**


Given the sequence 2, 3, 1, 4 and the integer 3,


The possible subsequences of length 3 are as follows:


2, 3, 1 and 3, 1, 4


The inversions of the subsequence 2, 3, 1 are (2,1) and (3,1). So the number of inversions is 2.


Similarly, the inversion of the subsequence 3, 1, 4 is (3,1). So the number of inversions in this subsequence is 1.


So the maximum value of the number of inversions in a particular subsequence of length 3 is 2.


**Input format**

The first line of input consists of an integer T which is the number of test cases. Then T lines follow with the input format as follows:


n k x<sub>1</sub> x<sub>2</sub> … x<sub>n</sub>


Here n is the length of the original sequence, k is the length of the subsequences under consideration, and the xi(s) are the elements of the original sequence.


**Output format**

For each test case, output the value of M<sub>m</sub> as defined above.


**Sample Input**
```
1
4 3 2 3 1 4
```
**Sample Output**
```
2
```
